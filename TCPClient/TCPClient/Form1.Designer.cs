﻿namespace TCPClient
{
    partial class TCPClient
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TCPClient));
            this.lbCom = new System.Windows.Forms.ListBox();
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.msg = new System.Windows.Forms.TextBox();
            this.send = new System.Windows.Forms.Button();
            this.bwConnection = new System.ComponentModel.BackgroundWorker();
            this.messageList = new System.Windows.Forms.WebBrowser();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbCom
            // 
            this.lbCom.FormattingEnabled = true;
            this.lbCom.ItemHeight = 16;
            this.lbCom.Items.AddRange(new object[] {
            "Client"});
            this.lbCom.Location = new System.Drawing.Point(20, 15);
            this.lbCom.Margin = new System.Windows.Forms.Padding(4);
            this.lbCom.Name = "lbCom";
            this.lbCom.Size = new System.Drawing.Size(647, 52);
            this.lbCom.TabIndex = 4;
            this.lbCom.SelectedIndexChanged += new System.EventHandler(this.lbCom_SelectedIndexChanged);
            // 
            // bgw
            // 
            this.bgw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_DoWork);
            // 
            // msg
            // 
            this.msg.Location = new System.Drawing.Point(16, 466);
            this.msg.Margin = new System.Windows.Forms.Padding(4);
            this.msg.Name = "msg";
            this.msg.Size = new System.Drawing.Size(543, 22);
            this.msg.TabIndex = 6;
            this.msg.TextChanged += new System.EventHandler(this.msg_TextChanged);
            this.msg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.msg_KeyDown);
            // 
            // send
            // 
            this.send.Location = new System.Drawing.Point(568, 463);
            this.send.Margin = new System.Windows.Forms.Padding(4);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(100, 28);
            this.send.TabIndex = 7;
            this.send.Text = "send";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // bwConnection
            // 
            this.bwConnection.WorkerSupportsCancellation = true;
            this.bwConnection.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwConnection_DoWork);
            // 
            // messageList
            // 
            this.messageList.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.messageList.ContextMenuStrip = this.contextMenuStrip1;
            this.messageList.IsWebBrowserContextMenuEnabled = false;
            this.messageList.Location = new System.Drawing.Point(16, 116);
            this.messageList.Margin = new System.Windows.Forms.Padding(4);
            this.messageList.MinimumSize = new System.Drawing.Size(27, 25);
            this.messageList.Name = "messageList";
            this.messageList.Size = new System.Drawing.Size(652, 307);
            this.messageList.TabIndex = 8;
            this.messageList.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.messageList_DocumentCompleted_1);
            this.messageList.CausesValidationChanged += new System.EventHandler(this.messageList_CausesValidationChanged);
            this.messageList.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.messageList_ControlAdded);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.clearToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 52);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // Button1
            // 
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Button1.Location = new System.Drawing.Point(20, 431);
            this.Button1.Margin = new System.Windows.Forms.Padding(4);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(100, 28);
            this.Button1.TabIndex = 9;
            this.Button1.Text = "B";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(129, 432);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 10;
            this.button2.Text = "I";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(643, 430);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(24, 28);
            this.button3.TabIndex = 11;
            this.button3.Text = "😀";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.emojiButton_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(613, 430);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(24, 28);
            this.button4.TabIndex = 12;
            this.button4.Text = "❤️";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.emojiButton_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(583, 430);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(24, 28);
            this.button5.TabIndex = 13;
            this.button5.Text = "😘";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.emojiButton_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(553, 430);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(24, 28);
            this.button6.TabIndex = 14;
            this.button6.Text = "🔥";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.emojiButton_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(529, 74);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(138, 35);
            this.button7.TabIndex = 15;
            this.button7.Text = "Change server";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(249, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Commands: /active , /priv , /time , /nick\r\n";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button8.Location = new System.Drawing.Point(237, 432);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(100, 28);
            this.button8.TabIndex = 17;
            this.button8.Text = "u";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // TCPClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(684, 506);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.messageList);
            this.Controls.Add(this.send);
            this.Controls.Add(this.msg);
            this.Controls.Add(this.lbCom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TCPClient";
            this.Text = "TCP Client";
            this.Load += new System.EventHandler(this.TCPClient_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox lbCom;
        private System.ComponentModel.BackgroundWorker bgw;
        private System.Windows.Forms.TextBox msg;
        private System.Windows.Forms.Button send;
        private System.ComponentModel.BackgroundWorker bwConnection;
        private System.Windows.Forms.WebBrowser messageList;
        private System.Windows.Forms.Button Button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button8;
    }
}


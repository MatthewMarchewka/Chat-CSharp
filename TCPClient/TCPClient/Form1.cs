﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;


namespace TCPClient
{
    public partial class TCPClient : Form
    {
        public string username = "";
        public StreamWriter Sw = null;
        public StreamReader Sr = null;
        public TcpClient socket = null;

        public TCPClient(String nick, TcpClient newSocket, StreamWriter newSw)
        {
            InitializeComponent();
            username = nick;
            Sw = newSw;
            socket = newSocket;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }


        private bool activeCall = false;

        private void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string msgRecived;
                while ((msgRecived = Sr.ReadLine()) != "END")
                {
                    messageList.Invoke(new MethodInvoker(delegate { messageList.DocumentText = messageList.DocumentText + DateTime.Now.ToShortTimeString() + msgRecived + "<hr>"; }));
                }
                socket.Close();
            }
            catch (Exception ex)
            {
                activeCall = false;
                //MessageBox.Show(ex.ToString());
            }
        }

        private void send_Click(object sender, EventArgs e)
        {
            try
            {
                string msgSend = msg.Text;
                //messageList.DocumentText = messageList.DocumentText + DateTime.Now.ToShortTimeString() + " <b>"+username+"</b>: " + msg.Text + "<hr>";
                msg.Text = "";
                Sw.WriteLine(msgSend);
                Sw.Flush();
            }
            catch //(Exception ex)
            {
                lbCom.Invoke(new MethodInvoker(delegate { lbCom.Items.Add("blad: nie udalo sie nawiazac  polaczenia!"); }));
                activeCall = false;
                //MessageBox.Show(ex.ToString());
            }
        }

        private void bwConnection_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Sr = new StreamReader(socket.GetStream());
                activeCall = true;
                bgw.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                lbCom.Invoke(new MethodInvoker(delegate { lbCom.Items.Add("blad: nie udalo sie nawiazac  polaczenia!"); }));
                activeCall = false;
                MessageBox.Show(ex.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            msg.Text += "<b></b>";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            msg.Text += "<i></i>";
        }
        private void button8_Click(object sender, EventArgs e)
        {
            msg.Text += "<u></u>";
        }

        private void msg_TextChanged(object sender, EventArgs e)
        {

        }

        private void msg_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                send.PerformClick();
                e.Handled = true;
                e.SuppressKeyPress = true;
            };
        }

        private void messageList_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            messageList.Document.Window.ScrollTo(0, messageList.Document.Window.Size.Height);
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            messageList.Invoke(new MethodInvoker(delegate { messageList.DocumentText = ""; }));
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "HTML Files|*.html";
            sfd.Title = "Zapisz swoja rozmowe!";
            if(sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, messageList.Document.Body.Parent.OuterHtml);
            }
        }

        private void nudPort_ValueChanged(object sender, EventArgs e)
        {

        }

        private void TCPClient_Load(object sender, EventArgs e)
        {
            bwConnection.RunWorkerAsync();
        }

        private void messageList_ControlAdded(object sender, ControlEventArgs e)
        {
        }

        private void emojiButton_Click(object sender, EventArgs e)
        {
            msg.Text += (sender as Button).Text;
        }

        private void messageList_CausesValidationChanged(object sender, EventArgs e)
        {

        }

        private void messageList_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (messageList.Document != null)
            {
                messageList.Invoke(new MethodInvoker(delegate { messageList.Document.Window.ScrollTo(0, messageList.Document.Body.ScrollRectangle.Height); }));
            }
        }

        private void lbCom_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            socket.Close();
            this.Hide();
            Form2 okienko = new Form2();
            okienko.Show();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
